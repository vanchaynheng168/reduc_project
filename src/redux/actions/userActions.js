import actionsType from './actionType'

export const getAllUsers = (listUsers) => (
    {
        type: actionsType.getAllUsers,
        payLoad: listUsers
    }
)

export const addUser = (user) => (
    {
        type: actionsType.addUser,
        payLoad: { user }
    }
)

