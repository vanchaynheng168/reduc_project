import actionsType from './actionType'
import Axios from 'axios';

//--- get all posts from jsonplaceholder server
export const getAllPosts = () => async dispatch => {
    try {
        setLoading();

        //--- using fetch method
        /* const res = await fetch('https://jsonplaceholder.typicode.com/posts?_limit=10');
        const data = await res.json(); */

        const res = await Axios.get('https://jsonplaceholder.typicode.com/posts', { timeout: 5000 });
    
        let data = null;

        if(res.status === 200) {
            data = res.data;
        }
        
        dispatch({
            type: actionsType.getAllPosts,
            payload: data
        });
    }
    catch(err) {
        // console.log(err.response.status);
        dispatch({
            type: actionsType.postError,
            payload: err.response.status
        })
    }
    
};

//--- add new post to jsonplaceholder server
export const addPost = (post) => async dispatch => {
    try {
        setLoading();

        //--- using fetch method
        /* const res = await fetch('https://jsonplaceholder.typicode.com/posts', {
            method: 'POST',
            body: JSON.stringify(post),
            headers: {
                'Content-Type': 'application/json'
            }
        });
        const data = await res.json(); */

        const res = await Axios.post('https://jsonplaceholder.typicode.com/posts', post, {
            data: JSON.stringify(post),
            headers: {
                'Content-type': 'application/json'
            },
            timeout: 5000,
        })

        let data = null;
        
        if(res.status === 201) {
            data = res.data;
        }

        dispatch({
            type: actionsType.addPost,
            payload: data
        });
    }
    catch(err) {
        dispatch({
            type: actionsType.postError,
            payload: err.response.status
        })
    }
    
};

//--- delete post from jsonplaceholder server
export const deletePost = (id) => async dispatch => {
    try {
        setLoading();

        //--- using fetch method
        /* await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`, {
            method: 'DELETE'
        }); */

        await Axios.delete(`https://jsonplaceholder.typicode.com/posts/${id}`, { timeout: 5000 });

        // delete status 204

        dispatch({
            type: actionsType.deletePost,
            payload: id
        });
    }
    catch(err) {
        dispatch({
            type: actionsType.postError,
            payload: err.response.status
        })
    }
    
};

//--- get post by id from jsonplaceholder server
export const getPostById = (id) => async dispatch => {
    try {
        setLoading();
        
        //--- using fetch method
        /* const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
        const data = await res.json(); */
        
        const res = await Axios.get(`https://jsonplaceholder.typicode.com/posts/${id}`, { timeout: 5000 })

        let data = null;

        if(res.status === 200) {
            data = res.data;
        }

        dispatch({
            type: actionsType.getPostById,
            payload: data
        });
    }
    catch(err) {
        dispatch({
            type: actionsType.postError,
            payload: err.response.status
        })
    }
    
};

//--- update post from jsonplaceholder server
export const updatePost = (post) => async dispatch => {
    try {
        setLoading();
    
        //--- fetch method
        /* const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${post.id}`, {
            method: 'PUT',
            body: JSON.stringify(post),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            }
        });
        const data = await res.json(); */
        
        const res = await Axios.patch(`https://jsonplaceholder.typicode.com/posts/${post.id}`, post, {
            data: JSON.stringify(post),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            timeout: 5000,
        })

        let data = null;

        if(res.status === 200) {
            data = res.data;
        }

        dispatch({
            type: actionsType.updatePost,
            payload: data
        });
    }
    catch(err) {
        dispatch({
            type: actionsType.postError,
            payload: err.response.status
        })
    }
    
};

//--- set loading to true
export const setLoading = () => {
    return {
        type: actionsType.setLoading
    }
}


//--- using axios library 
/* Axios({
    url: 'https://jsonplaceholder.typicode.com/posts?_limit=10',
    method: 'GET',
    timeout: 5000,
})
.then(res => this.setState({ loading: false, posts: res.data}))
.catch(err => { 
    if(err.response) {
        if (err.response.status === 404) {
            alert('Error: Page Not Found');
            this.setState({ loading: false });
        }
    }
    console.log(err);
    this.setState({ loading: false });
}); */

//--- other additional of axios library
/* Axios.get("https://jsonplaceholder.typicode.com/posts?_limit=10", { timeout: 5000 })
    .then(res => { 
        this.setState({ loading: false, posts: res.data})
        getAllPosts(res.data); 
    })
    .catch(err => {
        if(err.response) { 
            if (err.response.status === 404) {
                alert('Error: Page Not Found');
                this.setState({ loading: false });
            }
            else if (err.request) {
                // Request was made but no response
                console.error(err.request);
                this.setState({ loading: false });
            } 
            else {
                console.error(err.message);
                this.setState({ loading: false });
            }
        }
        console.log(err);
        this.setState({ loading: false });
    }); */