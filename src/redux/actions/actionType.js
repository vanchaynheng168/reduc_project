const actionsType = {
    getAllPosts: "getAllPosts",
    addPost: "addPost",
    deletePost: "deletePost",
    updatePost: "updatePost",
    getPostById: "getPostById",
    
    getAllUsers: "getAllUsers", 
    addUser: "addUser",
    deleteUser: "deleteUser",
    updateUser: "updateUser",
    getUserById: "getUserById",
    
    postError: "postError",
    userError: "userError",

    setLoading: "setLoading",
}
export default actionsType;