import actionsType from "../../actions/actionType"

const initState = {
    users: [],
}

export const userReducer = (state = initState, action) => {
    switch (action.type) {
        case actionsType.getAllUsers: {
            const listUsers = action.payLoad.listUsers;

            return { users: [...state.users, listUsers] }
        }

        case actionsType.addUser: {
            const user = action.payLoad.user;
            
            return { users: [...state.users, user] }
        }

        // case actionsType.updateUser: {
        //     const updateCounter = [...state.counters]
        //     var newCount = updateCounter[action.payLoad].count
        //     var increase = updateCounter[action.payLoad].increase
        //     updateCounter[action.payLoad].count = newCount + increase
        //     return { counters: [...updateCounter] }
        // }

        // case actionsType.viewUser: {
        //     const updateCounter = [...state.counters]
        //     var count = updateCounter[action.payLoad].count
        //     var inc = updateCounter[action.payLoad].increase
        //     updateCounter[action.payLoad].count = count - inc
        //     return { counters: [...updateCounter] }
        // }

        // case actionsType.deleteUser: {
        //     return { counters: [...state.counters.filter((c) => c.id !== action.payLoad)] }
        // }

        default:
            return state
    }
}

export default userReducer;