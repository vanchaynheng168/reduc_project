import actionsType from "../../actions/actionType"

const initState = {
    posts: null,
    current: null,
    loading: false,
    error: null,
}

export const postReducer = (state = initState, action) => {
    switch (action.type) {
        case actionsType.getAllPosts: {
            return { 
                ...state, 
                posts: action.payload,
                loading: false 
            }
        }

        case actionsType.addPost: {
            return {
                ...state,
                posts: [...state.posts, action.payload],
                loading: false
            }
        }

        case actionsType.deletePost: {
            return {
                ...state,
                posts: state.posts.filter(post => post.id !== action.payload),
                loading: false
            }
        }

        case actionsType.getPostById: {
            return {
                ...state,
                current: action.payload,
                loading: false
            }
        }

        case actionsType.updatePost: {
            return {
                ...state,
                posts: state.posts.map(post => post.id === action.payload.id ? action.payload : post),
            }
        }

        case actionsType.setLoading: {
            return {
                ...state,
                loading: true
            }
        }

        case actionsType.postError: {
            console.error(action.payload);
            return {
                ...state,
                error: action.payload
            }
        }

        default:
            return state
    }
}

export default postReducer;