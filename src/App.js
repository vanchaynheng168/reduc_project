import React from 'react';
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";
import ListPosts from './components/pages/posts/ListPosts';
import CreatePost from './components/pages/posts/CreatePost';
import ListUsers from './components/pages/users/ListUsers';
import CreateUser from './components/pages/users/CreateUser';
import UpdateUser from './components/pages/users/UpdateUser';
import NavbarLayout from "./components/layouts/NavbarLayout";
import { Home } from './components/pages/Home';
import UpdatePost  from './components/pages/posts/UpdatePost';

const App = () => {
  return (
    <div className="container">
      <Router>
        <NavbarLayout />
          <Switch>
            
            <Route exact path="/" component={Home} />
            
            <Route exact path="/posts" component={ListPosts} />
            <Route exact path="/posts/create" component={CreatePost} />
            <Route exact path="/posts/update/:id" component={UpdatePost} />

            <Route exact path="/users" component={ListUsers} />
            <Route exact path="/users/create" component={CreateUser} />
            <Route exact path="/users/update/:id" component={UpdateUser} />

            <Route path="*" render={() => <h1 className="text-center">404 Pages, NotFound!</h1>}/>

          </Switch>
        <h3 className="bg-dark text-white text-center pt-2 pb-3">Copyright &copy; 2020, SVAY KONG</h3>
      </Router>
    </div>
  );
}

export default App;
