import React, { Component } from 'react'
import { Form, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import PropTypes from "prop-types";
import { addPost } from "../../../redux/actions/postActions";

export class CreatePost extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: false,
            title: "",
            body: "",
        }
    }
    
    onChange = (event) => {
        const name = event.target.name;
        const value = event.target.value;
        this.setState({ [name]: value })
    }

    onSubmit = (event) => {
        event.preventDefault();
        const newPost = {}; 
        newPost.title = this.state.title;
        newPost.body = this.state.body;
        this.props.addPost(newPost);
        this.setState({ loading: true, title: '', body: ''});
        setTimeout(() => {
            alert("Add post successfully!");
            this.setState({ loading: false });
        }, 5000);
    }

    render() {
        const { loading, title, body } = this.state;
        if(loading) {
            return <h1>Loading...</h1>;
        }
        return (
            <div className="py-3">
                <Link to='/posts'>GoBack</Link>
                <h1>Create Post</h1>
                <Form onSubmit={this.onSubmit}>
                    <Form.Group>
                        <Form.Label>Title</Form.Label>
                        <Form.Control type="text" 
                            placeholder="Enter title" 
                            name="title" 
                            onChange={this.onChange}
                            value={title}/>
                    </Form.Group>
                    <Form.Group>
                        <Form.Label>Body</Form.Label>
                        <Form.Control as="textarea" 
                            rows="3" 
                            name="body" 
                            onChange={this.onChange} 
                            placeholder="Enter body"
                            value={body}/>
                    </Form.Group>
                    <Button variant="primary" type="submit">Submit</Button>
                </Form>
            </div>
        )
    }
}

CreatePost.propTypes = {
    addPost: PropTypes.func.isRequired
}

export default connect(null, { addPost })(CreatePost);

//---- define post method with fetch method
    /* postData = async (url='', data = {}) => {
        const response = await fetch(url, {
            method: 'POST',
            mode: 'cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
            'Content-Type': 'application/json'
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data), // body data type must match "Content-Type" header
        });
        return response; // parses JSON response into native JavaScript objects
    } */

/* this.postData("https://jsonplaceholder.typicode.com/posts", { title: this.state.title, body: this.state.body })
    .then(res => { 
        if(res.status === 201) {
            alert("Create post successfully!");
            return res.json();
        }
        else {
            alert("Create post failed!");
        } 
        this.setState({ loading: false });
    })
    .then(result => console.log(result))
    .catch(err => console.log(err)); */

/* Axios.post("https://jsonplaceholder.typicode.com/posts", newPost, 
    { timeout: 5000, headers: { 'Content-Type': 'application/json' }, data: JSON.stringify(newPost) })
        .then(res => { 
            if(res.status === 201) {
                alert("Create post successfully!");
                this.setState({ loading: false });
                return res.data;
            }
        })
        .then(data => { 
            console.log(data);
            addPost(data); 
        })
        .catch(err => {
            if(err.response) {
                if(err.response.status === 404) {
                    alert("404 Page, Not Found!");
                    this.setState({ loading: false });
                }
                else {
                    alert("Create post failed!'");
                    this.setState({ loading: false });
                }
            }
            console.log(err);
            this.setState({ loading: false });
        }); */