import React, { useState } from 'react'
import { Button } from 'react-bootstrap';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deletePost } from '../../../redux/actions/postActions';
import PropTypes from 'prop-types';

const PostItem = ({ post, deletePost }) => {
    const [loading, setLoading] = useState(false);
    const onDelete = () => {
        deletePost(post.id);
        setLoading(true);
        setTimeout(() => {
            alert('Delete post successfully!');
            setLoading(false);
        }, 5000);
    }
    if(loading) {
        return <h1>Loading...</h1>;
    }
    return (
        <div>
            <h1>{post.id}</h1>
            <h3>{post.title}</h3>
            <p>{post.body}</p>
            <div>
                <Button variant="dark" onClick={() => alert(`id: ${post.id} title: ${post.title}`)}>View</Button>
                <Button className="mx-1" variant="warning" as={Link} to={`/posts/update/${post.id}`}>Update</Button>
                <Button variant="danger" onClick={onDelete}>Delete</Button>
            </div>
        </div>
    )
}

PostItem.propTypes = {
    post: PropTypes.object.isRequired,
    deletePost: PropTypes.func.isRequired,
}

export default connect(null, { deletePost })(PostItem);
