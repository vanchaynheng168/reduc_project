import React, { Component } from 'react'
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import PostItem  from './PostItem';
import PropTypes from 'prop-types';
import { getAllPosts } from "../../../redux/actions/postActions";

class ListPosts extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            loading: true,
            posts: null,  
        }
    }

    componentDidMount() {
        const { getAllPosts } = this.props;
        getAllPosts();
    }

    componentDidUpdate() {
        const { posts, loading } = this.props.post;
        if(this.state.posts !== posts) {
            this.setState({ posts, loading });
        }
    }

    render() {
        const { loading, posts } = this.state;
        
        if(loading || posts === null) {
            return <h1>Loading...</h1>;
        }
        return (
            <div className="py-3">
                <Link to="/posts/create">Create Post</Link>
                { !loading && posts.length === 0 ? (<h3>No data yet...</h3>
                ) : (
                    posts.map(post => <PostItem post={post} key={post.id}/>)
                )}
            </div>
        )
    }
}

ListPosts.propTypes = {
    post: PropTypes.object.isRequired,
    getAllPosts: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
    post: state.postReducer
});

export default connect(mapStateToProps, { getAllPosts })(ListPosts);
